=== VFH Soundcloud ===
Contributors: cgrymala
Tags: soundcloud, embed
Donate link: http://virginiahumanities.org/
Requires at least: 4.6
Tested up to: 4.7
Stable tag: 0.1
License: GPL v2 or higher
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin adds some additional functionality to the native Soundcloud oEmbed in WordPress.

== Description ==
This plugin attempts to bridge some of the gaps between the functionality offered by the [Soundcloud is Gold](https://wordpress.org/plugins/soundcloud-is-gold/) plugin and the native WordPress oEmbed functionality.

Features added to the native oEmbed functionality by this plugin are:

* Implements the `[soundcloud]` shortcode, with the following attributes:
    * Accepts Soundcloud ID or URL as the identifier for the clip
    * Whether to auto-play the clip or not
    * Whether to display Soundcloud comments in the embed
    * Whether to display the Soundcloud artwork in the embed
    * The width of the player
    * Additional CSS classes to apply to the embed wrapper
    * Which type of player to embed (Standard, Artwork, Mini or HTML5)
    * The base color of the embedded player

== Installation ==
* Upload the vfh-soundcloud folder to wp-content/plugins
* Locate the VFH Soundcloud plugin in the list of available plugins on your site
* Activate the plugin
* Visit the settings page to set up some default options

== Frequently Asked Questions ==
= How do I use the shortcode? =

Full usage of the shortcode looks something like:

`[soundcloud url="https://soundcloud.com/celldweller/scandroid-salvation-code-1" artwork=0 comments=1 autoplay=0 width="500" classes="some-extra-css-classes more-extra-css-classes" playertype="html5" color="7ee6dd" format="tracks"]`

_or_

`[soundcloud id="285844194" artwork=0 comments=1 autoplay=0 width="500" classes="some-extra-css-classes more-extra-css-classes" playertype="html5" color="7ee6dd" format="tracks"]`

The parameters are as follows:

* `id` - If you have the Soundcloud ID for the clip, provide that in this parameter
* `url` - If you do not have the Soundcloud ID, or would simply prefer to use the URL, provide that in this parameter
* `artwork` - If you would like to show the artwork, set this parameter to 1; if you would like to hide the artwork, set it to 0
* `comments` - If you would like to show the Soundcloud comments, set this to 1; if you would like to hide them, set it to 0
* `autoplay` - If you want the clip to play automatically once it's loaded, set this to 1; otherwise, set it to 0
* `width` - If you would like the clip to be a specific width on the page, set that width here
* `classes` - If you have any extra CSS classes you would like to add to the embed wrapper, set them here
* `playertype` - Accepts the following values:
    * `html5`
    * `standard`
    * `artwork` 
    * `mini`
* `color` - provide the hex code for the color you would like to be the base of the player
* `format` - accepts the following values:
    * `tracks` - embed a single track
    * `users` - embed a user profile
    * `playlists` - embed a full playlist or album

== Changelog ==
= 0.1 =
This is the first version of this plugin