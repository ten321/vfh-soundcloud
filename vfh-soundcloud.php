<?php
/*
Plugin Name: VFH Soundcloud
Plugin URI: http://backstoryradio.org/
Description: Adds some functionality to the way Soundcloud items are embedded in WordPress
Version: 0.1
Author: Ten-321 Enterprises
Author URI: http://ten-321.com/
Text Domain: vfh-soundcloud
Domain Path: /languages
*/
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

if ( ! class_exists( 'VFH_Soundcloud' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . '/classes/class-vfh-soundcloud.php' );
	VFH_Soundcloud::instance();
}