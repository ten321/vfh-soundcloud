<?php
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * Implements the main functionality of the VFH Soundcloud plugin
 */
if ( ! class_exists( 'VFH_Soundcloud' ) ) {
	class VFH_Soundcloud {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  0.1
		 * @access public
		 * @var    string
		 */
		public $version = '0.1';
		
		/**
		 * Holds the class instance.
		 *
		 * @since   0.1
		 * @access	private
		 * @var		VFH_Soundcloud
		 */
		private static $instance;
		
		/**
		 * Holds the textdomain tag
		 * 
		 * @since   0.1
		 * @access  public
		 * @var     string
		 */
		public $textdomain = 'vfh-soundcloud';
		
		/**
		 * Holds the slug for our settings page
		 *
		 * @since  0.1
		 * @access public
		 * @var    string
		 */
		public $settings_page = 'vfh-soundcloud';
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   0.1
		 * @return	VFH_Soundcloud
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className = __CLASS__;
				self::$instance = new $className;
			}
			return self::$instance;
		}
		
		/**
		 * Construct the object
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		function __construct() {
			add_action( 'plugins_loaded', array( $this, 'startup' ) );
			add_action( 'init', array( $this, 'init' ) );
			add_action( 'admin_init', array( $this, 'admin_init' ) );
			add_action( 'admin_menu', array( $this, 'admin_menu' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts_and_styles' ) );
		}
		
		/**
		 * Perform any actions that need to happen once this plugin is loaded
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		function startup() {
			if ( shortcode_exists( 'soundcloud' ) ) {
				remove_shortcode( 'soundcloud' );
			}
			add_shortcode( 'soundcloud', array( $this, 'do_shortcode' ) );
		}
		
		/**
		 * Perform any actions that need to happen upon init
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		function init() {
			$this->load_textdomain();
			return;
		}
		
		/**
		 * Perform any actions that need to happen upon admin_init
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		function admin_init() {
			add_settings_section( $this->settings_page, __( 'VFH Soundcloud Settings' ), array( $this, 'settings_section' ), $this->settings_page );
			
			add_settings_field( 
				'vfh-soundcloud-artwork', 
				__( 'Show artwork by default?', $this->textdomain ), 
				array( $this, 'settings_field' ), 
				$this->settings_page, 
				$this->settings_page, 
				array( 
					'name' => 'artwork', 
					'type' => 'checkbox', 
					'options' => array( 1 => 'Yes' ) 
				)
			);
			
			add_settings_field( 
				'vfh-soundcloud-comments', 
				__( 'Show comments by default?', $this->textdomain ), 
				array( $this, 'settings_field' ), 
				$this->settings_page, 
				$this->settings_page, 
				array( 
					'name' => 'comments', 
					'type' => 'checkbox', 
					'options' => array( 1 => 'Yes' ) 
				) 
			);
			
			add_settings_field( 
				'vfh-soundcloud-autoplay', 
				__( 'Autoplay clips by default?', $this->textdomain ), 
				array( $this, 'settings_field' ), 
				$this->settings_page, 
				$this->settings_page, 
				array( 
					'name' => 'autoplay', 
					'type' => 'checkbox', 
					'options' => array( 1 => 'Yes' ) 
				) 
			);
			
			add_settings_field( 
				'vfh-soundcloud-playertype', 
				__( 'What type of player would you like to display by default?', $this->textdomain ), 
				array( $this, 'settings_field' ), 
				$this->settings_page, 
				$this->settings_page, 
				array( 
					'name' => 'playertype', 
					'type' => 'radio', 
					'options' => array( 
						'mini' => 'Mini', 
						'standard' => 'Standard', 
						'artwork' => 'Artwork', 
						'html5' => 'html5' 
					) 
				) 
			);
			
			add_settings_field( 
				'vfh-soundcloud-width', 
				__( 'Default player width:', $this->textdomain ), 
				array( $this, 'settings_field' ), 
				$this->settings_page, 
				$this->settings_page, 
				array( 
					'label_for' => $this->get_field_id( 'width' ), 
					'name' => 'width', 
					'type' => 'text' 
				) 
			);
			
			add_settings_field( 
				'vfh-soundcloud-color', 
				__( 'Default color:', $this->textdomain ), 
				array( $this, 'settings_field' ), 
				$this->settings_page, 
				$this->settings_page, 
				array( 
					'name' => 'color', 
					'label_for' => $this->get_field_id( 'color' ), 
					'type' => 'color' 
				) 
			);
			
			register_setting( 
				$this->settings_page, 
				'vfh-soundcloud-defaults', 
				array( $this, 'sanitize_settings' ) 
			);
			
			return;
		}
		
		/**
		 * Register the settings page for the plugin
		 *
		 * @access  public
		 * @since   0.1
		 * @return void
		 */
		function admin_menu() {
			add_options_page( 
				__( 'VFH Soundcloud', $this->textdomain ), 
				__( 'VFH Soundcloud', $this->textdomain ), 
				'manage_options', 
				$this->settings_page, 
				array( $this, 'do_settings_page' ) 
			);
		}
		
		/**
		 * Set up any JavaScript or CSS needed by this plugin
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		function add_scripts_and_styles() {
			return;
		}
		
		/**
		 * Register the plugin's textdomain to make this plugin
		 * 		l10n and i18n compatible
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		function load_textdomain() {
			load_plugin_textdomain( $this->textdomain, false, plugin_dir_path( dirname( __FILE__ ) ) . '/languages' );
		}
		
		/**
		 * Output the settings page
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		function do_settings_page() {
?>
<div class="wrap">
	<h2><?php _e( 'VFH Soundcloud Settings', $this->textdomain ) ?></h2>
	<form method="post" action="options.php">
<?php
			settings_fields( $this->settings_page );
			do_settings_sections( $this->settings_page );
?>
		<p><input type="submit" value="<?php _e( 'Save' ) ?>" class="button-primary"/></p>
    </form>
</div>
<?php
		}
		
		/**
		 * Placeholder for settings section
		 */
		function settings_section() {
			return;
		}
		
		/**
		 * Output a specific settings field
		 *
		 * @access  public
		 * @since   0.1
		 * @return  void
		 */
		function settings_field( $args=array() ) {
			if ( ! array_key_exists( 'type', $args ) ) {
				$args['type'] = 'text';
			}
			
			switch ( $args['type'] ) {
				case 'checkbox' : 
				case 'radio' : 
					foreach ( $args['options'] as $val => $label ) {
						$name = $this->get_field_name( $args['name'] );
						if ( 'checkbox' == $args['type'] ) {
							$name .= '[' . sanitize_title( $val ) . ']';
						}
						printf( '<p><input type="%1$s" value="%2$s" id="%3$s" name="%4$s"%6$s/><label for="%3$s">%5$s</label></p>', $args['type'], $val, sprintf( '%s_%s', $this->get_field_id( $args['name'] ), sanitize_title( $val ) ), $name, $label, checked( $this->get_option( $args['name'] ), $val, false ) );
					}
					break;
				default :
					$value = $this->get_option( $args['name'] );
					if ( 'color' == $args['type'] ) {
						$value = '#' . $value;
					}
					printf( '<p><input type="%4$s" value="%1$s" id="%2$s" name="%3$s"/></p>', $value, $args['label_for'], $this->get_field_name( $args['name'] ), $args['type'] );
					break;
			}
		}
		
		/**
		 * Retrieve a specific option for the plugin
		 * @param   string $name the name of the option to retrieve
		 *
		 * @access  private
		 * @since   0.1
		 * @return  mixed the value of the option
		 */
		private function get_option( $name='' ) {
			if ( empty( $name ) ) {
				return false;
			}
			
			if ( ! isset( $this->options ) ) {
				$this->options = get_option( 'vfh-soundcloud-defaults', array() );
			}
			
			if ( array_key_exists( $name, $this->options ) ) {
				return $this->options[$name];
			}
			
			return false;
		}
		
		/**
		 * Build an appropriate HTML ID for a specific option field
		 * @param   string the key from which to build the ID
		 * 
		 * @access  private
		 * @since   0.1
		 * @return  string the HTML ID
		 */
		function get_field_id( $key ) {
			return sprintf( '_vfh_soundcloud_defaults_%s', sanitize_title( $key ) );
		}
		
		/**
		 * Build an appropriate HTML name for a specific option field
		 * @param   string the key from which to build the name
		 * 
		 * @access  private
		 * @since   0.1
		 * @return  string the HTML name
		 */
		function get_field_name( $key ) {
			return sprintf( 'vfh-soundcloud-defaults[%s]', sanitize_title( $key ) );
		}
		
		/**
		 * Validate and sanitize the settings for this plugin
		 * @param   array $opts the array of settings to be sanitized
		 *
		 * @access  public
		 * @since   0.1
		 * @return  array the sanitized array of options
		 */
		function sanitize_settings( $opts=array() ) {
			$new_opts = array();
			foreach ( $opts as $k => $v ) {
				switch ( $k ) {
					case 'artwork' : 
					case 'autoplay' : 
					case 'comments' : 
						$new_opts[$k] = 1;
						break;
					case 'playertype' : 
						$new_opts[$k] = $v;
						break;
					case 'color' : 
						$new_opts[$k] = str_replace( '#', '', $v );
						break;
					default : 
						$new_opts[$k] = $v;
						break;
				}
			}
			
			if ( ! array_key_exists( 'artwork', $new_opts ) ) {
				$new_opts['artwork'] = 0;
			}
			
			if ( ! array_key_exists( 'autoplay', $new_opts ) ) {
				$new_opts['autoplay'] = 0;
			}
			
			if ( ! array_key_exists( 'comments', $new_opts ) ) {
				$new_opts['comments'] = 0;
			}
			
			return $new_opts;
		}
		
		/**
		 * Handle the soundcloud shortcode
		 * @param array $atts the list of attributes to be processed
		 * @param string $content any content that appears inside of the shortcode
		 *
		 * @access  public
		 * @since   0.1
		 * @return  string the rendered content
		 */
		function do_shortcode( $atts=array(), $content='' ) {
			if ( array_key_exists( 'params', $atts ) ) {
				return $this->do_native_shortcode( $atts );
			}
			
			if ( ! empty( $content ) && esc_url( $content ) ) {
				$atts['url'] = esc_url( $content );
			}
			
			if ( ! array_key_exists( 'url', $atts ) && array_key_exists( 'id', $atts ) && ! is_numeric( $atts['id'] ) ) {
				$atts['url'] = esc_url( $atts['id'] );
				unset( $atts['id'] );
			}
			
			$atts = $this->validate_shortcode_atts( shortcode_atts( $this->get_defaults(), $atts ) );
			
			if ( empty( $atts['id'] ) && empty( $atts['url'] ) ) {
				return $content;
			}
			
			//Player types sizes
			switch ( $atts['playertype'] ) {
				case 'standard':
					$height = ($format == 'tracks') ? '81px' : '165px';
					break;
				case 'artwork':
					$height = $atts['width'];
					break;
				case 'mini':
					$height = '18px';
					break;
				case 'html5':
					$height = ( 'tracks' == $atts['format'] ) ? '166px' : '450px';
					$html5Player = true;
					break;
			}
			
			$playerID = rand();
			
			if ( ! empty( $atts['id'] ) && is_numeric( $atts['id'] ) ) {
				$playerID = absint( $atts['id'] );
			} else if ( ! empty( $atts['url'] ) ) {
				$urlparts = explode( '/', $atts['url'] );
				$tmp = null;
				while ( empty( $tmp ) && count( $urlparts ) > 0 ) {
					$tmp = array_pop( $urlparts );
				}
				$playerID = sanitize_title( $tmp );
			}
			
			$player = sprintf( '<div class="soundcloud-embed %1$s" id="soundcloud-embed-%2$s">', $atts['classes'], $playerID );
			
			if ( isset( $atts['id'] ) && is_numeric( $atts['id'] ) && ! empty( $atts['id'] ) ) {
				if ( ! $html5Player ) {
					$embedurl = 'https://player.soundcloud.com/player.swf';

					$embedurl = add_query_arg( 'url', urlencode( sprintf( 'https://api.soundcloud.com/%1$s/%2$s/', $atts['format'], $atts['id'] ) ), $embedurl );

					if ( $atts['artwork'] ) {
						$embedurl = add_query_arg( 'show_artwork', 'true', $embedurl );
					} else {
						$embedurl = add_query_arg( 'show_artwork', 'false', $embedurl );
					}

					if ( $atts['autoplay'] ) {
						$embedurl = add_query_arg( 'auto_play', 'true', $embedurl );
					} else {
						$embedurl = add_query_arg( 'auto_play', 'false', $embedurl );
					}
					
					if ( $atts['comments'] ) {
						$embedurl = add_query_arg( 'show_comments', 'true', $embedurl );
					} else {
						$embedurl = add_query_arg( 'show_comments', 'false', $embedurl );
					}

					$embedurl = add_query_arg( 'player_type', $atts['playertype'], $embedurl );

					if ( isset( $atts['color'] ) && ! empty( $atts['color'] ) ) {
						$embedurl = add_query_arg( 'color', $atts['color'], $embedurl );
					}

					$player .= sprintf( '<object height="%1$s" width="%2$s">', $height, $atts['width'] );
					$player .= sprintf( '<param name="movie" value="%s"></param>', $embedurl );
					$player .= '<param name="allowscriptaccess" value="always"></param>';
					$player .= '<param name="wmode" value="transparent"></param>';
					$player .= sprintf( '<embed wmode="transparent" allowscriptaccess="always" height="%1$s" src="%2$s" type="application/x-shockwave-flash" width="%3$s"></embed>', $height, $embedurl, $atts['height'] );
					$player .= '</object>';
				} else {
					$embedurl = 'https://w.soundcloud.com/player/';
					$embedurl = add_query_arg( 'url', urlencode( sprintf( 'https://api.soundcloud.com/%1$s/%2$s', $atts['format'], $atts['id'] ) ), $embedurl );

					if ( $atts['artwork'] ) {
						$embedurl = add_query_arg( 'show_artwork', 'true', $embedurl );
					} else {
						$embedurl = add_query_arg( 'show_artwork', 'false', $embedurl );
					}

					if ( $atts['autoplay'] ) {
						$embedurl = add_query_arg( 'auto_play', 'true', $embedurl );
					} else {
						$embedurl = add_query_arg( 'auto_play', 'false', $embedurl );
					}
					
					if ( $atts['comments'] ) {
						$embedurl = add_query_arg( 'show_comments', 'true', $embedurl );
					} else {
						$embedurl = add_query_arg( 'show_comments', 'false', $embedurl );
					}

					$embedurl = add_query_arg( 'player_type', $atts['playertype'], $embedurl );

					if ( isset( $atts['color'] ) && ! empty( $atts['color'] ) ) {
						$embedurl = add_query_arg( 'color', $atts['color'], $embedurl );
					}

					$player .= sprintf( '<iframe width="%1$s" height="%2$s" scrolling="no" frameborder="no" src="%3$s"></iframe>', $atts['width'], $height, $embedurl );
				}
			} else {
				$embedurl = 'https://soundcloud.com/oembed';
				$embedurl = add_query_arg( 'format', 'json', $embedurl );
				$embedurl = add_query_arg( 'url', urlencode( $atts['url'] ), $embedurl );
				
				if ( $atts['artwork'] ) {
					$embedurl = add_query_arg( 'show_artwork', 'true', $embedurl );
				} else {
					$embedurl = add_query_arg( 'show_artwork', 'false', $embedurl );
				}
				
				if ( $atts['autoplay'] ) {
					$embedurl = add_query_arg( 'auto_play', 'true', $embedurl );
				} else {
					$embedurl = add_query_arg( 'auto_play', 'false', $embedurl );
				}
				
				$embedurl = add_query_arg( 'player_type', $atts['playertype'], $embedurl );
				
				if ( $atts['comments'] ) {
					$embedurl = add_query_arg( 'show_comments', 'true', $embedurl );
				} else {
					$embedurl = add_query_arg( 'show_comments', 'false', $embedurl );
				}
				
				if ( isset( $atts['color'] ) && ! empty( $atts['color'] ) ) {
					$embedurl = add_query_arg( 'color', $atts['color'], $embedurl );
				}
				
				if ( isset( $atts['width'] ) ) {
					add_query_arg( 'maxwidth', $atts['width'], $embedurl );
					add_query_arg( 'maxheight', $height, $embedurl );
				}
				
				$request = wp_remote_get( $embedurl );
				if ( is_array( $request ) ) {
					$embed = json_decode( wp_remote_retrieve_body( $request ) );
					
					if ( ! $atts['artwork'] ) {
						$embed->html = str_replace( 'visual=true', 'visual=false', $embed->html );
						$embed->html = str_replace( 'height="400"', 'height="166"', $embed->html );
					}
					
					$player .= $embed->html;
				}
			}
			
			$player .= '</div>';
			
			return $player;
		}
		
		/**
		 * For some reason, Soundcloud offers "WordPress" embed code that doesn't 
		 * 		seem to be supported in WordPress sites. It's possible it's strictly 
		 * 		a wordpress.com or JetPack feature, but we'll implement it just to be
		 * 		safe
		 *
		 * @access  private
		 * @since   0.1
		 * @return  string the rendered HTML code
		 */
		function do_native_shortcode( $atts=array() ) {
			if ( ! array_key_exists( 'url', $atts ) ) 
				return '';
			
			$urlparts = explode( '/', $atts['url'] );
			$tmp = null;
			while ( empty( $tmp ) && count( $urlparts ) > 0 ) {
				$tmp = array_pop( $urlparts );
			}
			$playerID = sanitize_title( $tmp );
			
			$player = sprintf( '<div class="soundcloud-embed%1$s" id="soundcloud-embed-%2$s">', '', $playerID );
			
			$embedurl = 'https://soundcloud.com/oembed';
			$embedurl = add_query_arg( 'format', 'json', $embedurl );
			$embedurl = add_query_arg( 'url', urlencode( $atts['url'] ), $embedurl );
			
			$visual = $this->get_option( 'artwork' );
			
			$params = explode( '&', $atts['params'] );
			foreach ( $params as $p ) {
				$parts = explode( '=', $p );
				if ( count( $parts ) > 2 ) {
					$key = array_shift( $parts );
					$val = implode( '=', $parts );
				} else {
					$key = $parts[0];
					$val = $parts[1];
				}
				
				if ( 'show_artwork' == $key ) {
					$visual = 'true' == $val;
				}
				
				$embedurl = add_query_arg( $key, $val, $embedurl );
			}
			
			if ( array_key_exists( 'width', $atts ) ) {
				add_query_arg( 'maxwidth', $atts['width'], $embedurl );
			}
			
			if ( array_key_exists( 'height', $atts ) ) {
				add_query_arg( 'maxheight', $atts['height'], $embedurl );
			}
			
			if ( array_key_exists( 'iframe', $atts ) ) {
				add_query_arg( 'iframe', $atts['iframe'], $embedurl );
			}
			
			$request = wp_remote_get( $embedurl );
			if ( is_array( $request ) ) {
				$embed = json_decode( wp_remote_retrieve_body( $request ) );
				
				if ( ! $visual ) {
					$embed->html = str_replace( 'visual=true', 'visual=false', $embed->html );
					$embed->html = str_replace( 'height="400"', 'height="166"', $embed->html );
				}

				$player .= $embed->html;
			}
			
			$player .= '</div>';
			
			return $player;
		}
		
		/**
		 * Set up the default shortcode attributes
		 *
		 * @access  private
		 * @since   0.1
		 * @return  array the array of attributes
		 */
		function get_defaults() {
			$atts = array(
				'id'         => 0, 
				'url'        => null, 
				'user'       => null, 
				'artwork'    => true, 
				'comments'   => false, 
				'autoplay'   => false, 
				'width'      => 0, 
				'classes'    => '', 
				'playertype' => 'html5', 
				'color'      => null, 
				'format'     => 'tracks', 
			);
			
			$options = get_option( 'vfh-soundcloud-defaults', array() );
			
			$atts = array_merge( $atts, $options );
			
			return apply_filters( 'vfh-soundcloud-default-atts', $atts );
		}
		
		/**
		 * Process the shortcode attributes and make sure they're valid
		 * @param array $atts the attributes being processed
		 *
		 * @access  private
		 * @since   0.1
		 * @return  array the validated array of attributes
		 */
		function validate_shortcode_atts( $atts=array() ) {
			if ( empty( $atts ) ) {
				return $atts;
			}
			
			foreach ( $atts as $k => $v ) {
				switch ( $k ) {
					case 'id' : 
						if ( is_numeric( $v ) ) {
							$atts[$k] = $v;
						} else {
							$atts[$k] = esc_url( $v );
						}
						break;
					case 'width' : 
						if ( stristr( $v, '%' ) ) {
							$atts[$k] = absint( str_replace( '%', '', $v ) ) . '%';
						} else if ( stristr( $v, 'px' ) ) {
							$atts[$k] = absint( str_replace( 'px', '', $v ) ) . 'px';
						} else {
							$atts[$k] = absint( $v );
						}
						break;
					case 'url' : 
						$atts[$k] = esc_url( $v );
						break;
					case 'artwork' : 
					case 'comments' : 
					case 'autoplay' : 
						$atts[$k] = in_array( $v, array( true, 'true', 1, '1', 'yes' ), true );
						break;
					case 'playertype' : 
						if ( ! in_array( strtolower( $v ), array( 'html5', 'standard', 'mini', 'artwork' ) ) ) {
							$atts[$k] = 'standard';
						} else {
							$atts[$k] = strtolower( $v );
						}
						break;
					case 'format' : 
						if ( ! in_array( strtolower( $v ), array( 'tracks', 'users', 'playlists' ) ) ) {
							if ( ! empty( $atts['user'] ) ) {
								$atts[$k] = 'users';
							} else {
								$atts[$k] = 'tracks';
							}
						} else {
							$atts[$k] = strtolower( $v );
						}
						break;
					case 'color' : 
						$atts[$k] = str_replace( '#', '', $v );
					default : 
						$atts[$k] = esc_attr( $v );
						break;
				}
			}
			
			return $atts;
		}
	}
}